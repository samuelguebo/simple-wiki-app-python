class EditPage(BlogHandler):
    def get(self):
        content = self.request.get('content')

        if content:
            p = Post(parent = blog_key(), content = content)
            p.put()
            self.redirect('/%s' % str(p.key().id()))
        else:
            error = "content, please!"
            self.render("newpage.html", content=content, error=error)
	

    def post(self):

